const express = require('express');
const app = express();
const puppeteer = require('puppeteer');
const ua = require("useragent");
const https = require('https');


function isBot(userAgent) {
    const agent = ua.is(userAgent);
    return !agent.webkit && !agent.opera && !agent.ie &&
        !agent.chrome && !agent.safari && !agent.mobile_safari &&
        !agent.firefox && !agent.mozilla && !agent.android;
}

function extractHostname(url) {
    var hostname;
    //find & remove protocol (http, ftp, etc.) and get hostname
    if (url.indexOf("://") > -1) {
        hostname = url.split('/')[2];
    } else {
        hostname = url.split('/')[0];
    }
    //find & remove port number
    hostname = hostname.split(':')[0];
    //find & remove "?"
    hostname = hostname.split('?')[0];
    return hostname;
}

const RENDER_CACHE = new Map();

/*const react_build_dir = "../build/"; // this variable refers to directory where our client-side React is built
app.use(react_build_dir); // this is to allow browser download the static files of the React app (CSS, JS, images).
*/
const port = 4000;

app.get('/csr', async(req, res) => {

    const fetch_url = req.query.url ? req.query.url : "";
    if (fetch_url) {
        puppeteer.launch().then(async browser => {
            const page = await browser.newPage();
            page.on('response', async resp => {
                if (resp.ok && resp.url() === fetch_url) {
                    var html = await resp.text();
                    res.send(html);
                }
            });

            await page.goto(fetch_url);

            browser.close();
        });
    }
});



app.get('/ssr', async(req, res) => {
    /*    if (!isBot(req.headers['user-agent'])) {
    		res.sendFile(react_build_dir + '/index.html'); // this is a human, send the HTML right away
    	} else { */
    try {
        const fetch_url = req.query.url ? req.query.url : "";
        if (fetch_url) {
            console.log(fetch_url);
            if (RENDER_CACHE.has(fetch_url)) {
                html = await RENDER_CACHE.get(fetch_url);
                res.send(html);
            } else {
                const browser = await puppeteer.launch({
                    headless: true
                });
                const page = await browser.newPage();

                // we need to override the headless Chrome user agent since its default one is still considered as "bot"
                await page.setUserAgent('Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36');
                await page.setRequestInterception(true);
                page.on('request', req => {
                    const blacklist = ['nexus.ensighten.com'];
                    if (blacklist.includes(extractHostname(req._url))) {
                        req.abort();
                    }
                    req.continue();
                });
                await page.goto(fetch_url, {
                    waitUntil: "load",
                });
                var html = await page.content();
                browser.close();
                RENDER_CACHE.set(fetch_url, html);
                res.send(html);
            }
        } else {
            res.send("Please pass query parameter as http://localhost:4000/ssr?url=https://www.example.com");
        }

    } catch (e) {
        console.log(e);
        res.send("ERROR");
    }

    /* }*/
});

app.listen(port, () => {
    console.log(`Web server is running at port ${port}`);
});